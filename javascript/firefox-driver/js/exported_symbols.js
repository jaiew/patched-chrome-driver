
goog.require('fxdriver.events');

var EXPORTED_SYMBOLS = [
  'Utils',
  'WebDriverError',
  'bot',
  'fxdriver',
  'goog',
  'notifyOfCloseWindow',
  'notifyOfSwitchToWindow',
  'webdriver'
];


package org.openqa.selenium.chrome;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableMap;
import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class ChromeTest {

  private static ChromeDriverService service;
  private WebDriver driver;

  @BeforeClass
  public static void createAndStartService() throws IOException{
    service = new ChromeDriverService.Builder()
        .usingChromeDriverExecutable(new File("/Users/jaiew/Downloads/chromedriver"))
        .usingAnyFreePort()
        .withEnvironment(ImmutableMap.of("DISPLAY",":20"))
        .build();
    service.start();
  }

  @AfterClass
  public static void createAndStopService() {
    service.stop();
  }

  @Before
  public void createDriver() {
    driver = new RemoteWebDriver(service.getUrl(),
        DesiredCapabilities.chrome());
  }

  @After
  public void quitDriver() {
    driver.quit();
  }

  @Test
  public void testGoogleSearch() {
    driver.get("http://www.google.com");
    WebElement searchBox = driver.findElement(By.name("q"));
    final String title = driver.getTitle();
    searchBox.sendKeys("webdriver");
    searchBox.submit();

    // Wait until the title changes
    new WebDriverWait(driver, 30).until(new Function<WebDriver, Boolean>() {
      public Boolean apply(WebDriver input) {
            return !input.getTitle().equals(title);
      }
    });

    assertEquals("webdriver - Google Search", driver.getTitle());
  }
}